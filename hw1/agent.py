import numpy as np
from math import sqrt

class Agent(object):

    def __init__(self, csvParameters, ksi=0.5, dhor = 10, timehor=5, goalRadiusSq=1, maxF = 10):
        """ 
            Takes an input line from the csv file,  
            and initializes the agent
        """
        self.id = int(csvParameters[0]) # the id of the agent
        self.gid = int(csvParameters[1]) # the group id of the agent
        self.pos = np.array([float(csvParameters[2]), float(csvParameters[3])]) # the position of the agent 
        self.vel = np.zeros(2) # the velocity of the agent
        self.goal = np.array([float(csvParameters[4]), float(csvParameters[5])]) # the goal of the agent
        self.prefspeed = float(csvParameters[6]) # the preferred speed of the agent
        self.gvel = self.goal-self.pos # the goal velocity of the agent
        self.gvel = self.gvel/(sqrt(self.gvel.dot(self.gvel )))*self.prefspeed       
        self.maxspeed = float(csvParameters[7]) # the maximum sped of the agent
        self.radius = float(csvParameters[8]) # the radius of the agent
        self.goalRadiusSq =goalRadiusSq # parameter to determine if agent is close to the goal
        self.atGoal = False # has the agent reached its goal?
        self.ksi = ksi # the relaxation time used to compute the goal force (in seconds, typically 0.5).
        self.dhor = dhor # the sensing radius
        self.timehor = timehor # the time horizon for computing avoidance forces, smoothly dropping to 0 as a time-to-collision approaches this time horizon.
        self.F = np.zeros(2) # the total force acting on the agent
        self.maxF = maxF # the maximum force that can be applied to the agent

        self.gForce = (self.gvel - self.vel) / self.ksi    #ksi is a certain characteristic time.

    #---------------------------------------
    def computeForces(self, neighbors=[]):
        """ 
            Your code to compute the forces acting on the agent. 
            You probably need to pass here a list of all the agents in the simulation to determine the agent's nearest neighbors
        """       
        if not self.atGoal:
            f = self.gForce                         #First aim for the goal.
            f += self.calcAvoidForce(neighbors)
            if self.magnitude(f) <= self.maxF:      #Restrict force by a maximum force.
                self.F = f
            else:
                self.F = np.zeros(2)

            self.gForce = (self.gvel - self.vel) / self.ksi    #Update new driving force.

    #---------------------------------------
    def update(self, dt):
        """ 
            Code to update the velocity and position of the agents.  
            as well as determine the new goal velocity 
        """
        if not self.atGoal:
            #Update the velocity but cap by the maximum speed.
            newVel = self.vel + self.F*dt
            if self.magnitude(newVel) <= self.maxspeed:
                self.vel = newVel
            #Else, don't update the Agent's velocity.

            self.pos += self.vel*dt   #update the position
        
            # compute the goal velocity for the next time step. do not modify this
            self.gvel = self.goal - self.pos
            distGoalSq = self.gvel.dot(self.gvel)
            if distGoalSq < self.goalRadiusSq: 
                self.atGoal = True  # goal has been reached
            else: 
                self.gvel = self.gvel/sqrt(distGoalSq)*self.prefspeed

    #---------------------------------------
    #Magnitude is the square root of the dot product of the vector with itself.
    #Separate function for readability of logic.
    def magnitude(self, vec):
        return np.sqrt(vec.dot(vec))

    #---------------------------------------
    def calcAvoidForce(self, neighbors):
        f = np.zeros(2)

        for n in neighbors:
            if n.id != self.id:
                ab_dir = self.pos-n.pos                         #Direction and magnitude from this Agent (a) to its neighbor (b).

                ab_dist = self.magnitude(ab_dir) - self.radius - n.radius
                ab_dir /= ab_dist                               #Normalize.

                if ab_dist < self.dhor:
                    t = self.timeToCollision(self.pos-n.pos, self.vel-n.vel, self.radius+n.radius)

                    if t is not None:
                        #Calculate new direction vector at time of predicted collision.
                        ab_dir = (self.pos + t*self.vel) - (n.pos + t*n.vel)
                        ab_dist = np.sqrt(ab_dir.dot(ab_dir))
                        ab_dir /= ab_dist

                        if t != 0:      #Don't divide by 0 (which is when there's already a collision).
                            avoidF = (max(self.timehor-t, 0) / t) * ab_dir
                        else:
                            avoidF = max(self.timehor, 0) * ab_dir

                        f += avoidF

        return f

    #---------------------------------------
    #Simple predictive time-to-collision of two agents using the quadratic formula to solve for a collision time.
    def timeToCollision(self, x, v, r):
        a = v.dot(v)
        b = x.dot(v)
        c = x.dot(x) - (r*r)
        d = (b*b) - (a*c)

        #If the magnitude of a is 0, then v is a zero vector, and both agents' velocities are the same (therefore colliding).
        if a == 0:
            return 0

        #If there is one solution (d<0) or one double-solution (d=0), no collision.
        if d <= 0:
            return None

        t_lower = (-b - np.sqrt(d)) / a
        t_upper = (-b + np.sqrt(d)) / a

        #If both solutions are negative, no collision.
        if t_lower < 0 and t_upper < 0:
            return None

        #If one is neg and the other non-neg, already colliding.
        if (t_lower < 0 and t_upper >= 0) or (t_upper < 0 and t_lower >= 0):
            return 0

        if (t_lower >= 0) and (t_upper >= 0):
            return min(t_lower, t_upper)        #Should be t_lower


