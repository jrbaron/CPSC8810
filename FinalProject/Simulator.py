import numpy as np
#from tkinter import *     #"tkinter" on a Python 3.5 install.
from Tkinter import *     #"tkinter" on a Python 3.5 install.
import time

import random
from Hummingbird import Hummingbird
from Hummingbird import randomVector

"""
    Initalize parameters to run a simulation
"""
dt = 0.05               #The simulation time step (seconds)
scenarioFile='8_agents.csv'
doExport = False        #Export the simulation?
trajectories = []       #Keep track of the agents' traces
ittr = 0                #Keep track of simulation iterations
maxIttr = 500           #How many time steps we want to simulate
globalTime = 0          #Simuation time
reachedGoals = False    #Have all agents reached their goals

""" 
    Drawing parameters
"""
pixelsize = 1024
framedelay = 30
drawVels = True
QUIT = False
paused = False
step = False
circles = []
velLines = []
gvLines = []

birds = []
numBirds = 3
birdRad = 0.2
feederPos = np.array([5, 5, 0])
feeder = Hummingbird(-1, feederPos, pb_rad=birdRad*2)

#---------------------------------------
def initialize():
    colors = ["#4b0082", "#e77645", "#d6dce2"]    #purple, orange, silver

    for i in range(numBirds):
        pos = np.array([3 + world_xmin + i,
               3 + world_ymin + i,
               0.0])
        vel = randomVector()
        #aggr = random.uniform(0.5, 4.0)   #Random float in given range
        aggr = random.random()
        pb_rad = birdRad
        ab_rad = pb_rad * ( 2*(1+aggr) )

        #Humm constructor sets additional properties like state.
        humm = Hummingbird(i, pos, vel, aggr, pb_rad, ab_rad, feederPos, colors[i%3])
        birds.append(humm)

#---------------------------------------
def initWorld(canvas):
    """
        initialize the agents 
    """
    print ("")
    print ("Simulation of Agents on a 2D plane.")
    print ("Green Arrow is Goal Velocity. Red Arrow is Current Velocity.")
    print ("SPACE to pause, 'S' to step frame-by-frame, 'V' to turn the velocity display on/off.")
    print ("")

    for b in birds:
        circles.append(canvas.create_oval(0, 0, b.PB_rad, b.PB_rad, fill=b.color))
        circles.append(canvas.create_oval(0, 0, b.AB_rad, b.AB_rad, outline="white"))
        velLines.append(canvas.create_line(0,0,10,10,fill="red"))
        gvLines.append(canvas.create_line(0,0,10,10,fill="green"))

    circles.append(canvas.create_oval(0, 0, feeder.PB_rad, feeder.PB_rad, fill="black"))  #For feeder

#---------------------------------------
def drawWorld():
    """
        draw the agents
    """

    for i in range(numBirds):
        humm = birds[i]

        #if not humm.atGoal:
        #Main bird representation (personal-bubble):
        canvas.itemconfig(circles[i*2], fill=humm.color)
        canvas.coords( circles[i*2],
                        world_scale*(humm.pos[0] - humm.PB_rad - world_xmin),
                        world_scale*(humm.pos[1] - humm.PB_rad - world_ymin),
                        world_scale*(humm.pos[0] + humm.PB_rad - world_xmin),
                        world_scale*(humm.pos[1] + humm.PB_rad - world_ymin) )

        #Attack-bubble outline:
        canvas.coords(  circles[i*2 +1],
                        world_scale*(humm.pos[0]- humm.AB_rad - world_xmin),
                        world_scale*(humm.pos[1] - humm.AB_rad - world_ymin),
                        world_scale*(humm.pos[0] + humm.AB_rad - world_xmin),
                        world_scale*(humm.pos[1] + humm.AB_rad - world_ymin) )

        canvas.coords(  velLines[i],
                            world_scale*(humm.pos[0] - world_xmin),
                            world_scale*(humm.pos[1] - world_ymin),
                            world_scale*(humm.pos[0]+ birdRad*humm.vel[0] - world_xmin),
                            world_scale*(humm.pos[1] + birdRad*humm.vel[1] - world_ymin) )

        #TODO: Goal velocity?
       # canvas.coords(  gvLines[i],
       #                     world_scale*(humm.pos[0] - world_xmin),
       #                     world_scale*(humm.pos[1] - world_ymin),
       #                     world_scale*(humm.pos[0]+ birdRad*humm.gvel[0] - world_xmin),
       #                     world_scale*(humm.pos[1] + birdRad*humm.gvel[1] - world_ymin) )

        if drawVels:
            canvas.itemconfigure(velLines[i], state="normal")
            canvas.itemconfigure(gvLines[i], state="normal")
        else:
            canvas.itemconfigure(velLines[i], state="hidden")
            canvas.itemconfigure(gvLines[i], state="hidden")

    #Draw last circle, the feeder.
    canvas.coords(  circles[-1],
                    world_scale*(feeder.pos[0] - feeder.PB_rad - world_xmin),
                    world_scale*(feeder.pos[1] - feeder.PB_rad - world_ymin),
                    world_scale*(feeder.pos[0] + feeder.PB_rad - world_xmin),
                    world_scale*(feeder.pos[1] + feeder.PB_rad - world_ymin) )

#---------------------------------------
def on_key_press(event):
    """
        keyboard events
    """                    
    global paused, step, QUIT, drawVels

    if event.keysym == "space":
        paused = not paused
    if event.keysym == "s":
        step = True
        paused = False
    if event.keysym == "v":
        drawVels = not drawVels
    if event.keysym == "Escape":
        QUIT = True

#---------------------------------------
#TODO: Changes to Hummingbirds as forces?
def updateSim(dt):
    for humm in birds:
        humm.computeForces(birds, ittr)

    for humm in birds:
        humm.update(dt)

#---------------------------------------
def drawFrame(dt):
        """
            simulate and draw frames
        """

        global start_time,step,paused,ittr,globalTime

    #if reachedGoals or ittr > maxIttr or QUIT: #Simulation Loop
    #    print("%s iterations ran... quitting."%ittr)
    #    win.destroy()
    #else:
        elapsed_time = time.time() - start_time
        start_time = time.time()
        if not paused:
            updateSim(dt)
            ittr += 1
            globalTime += dt
            for agent in birds:
                #if not agent.atGoal:
                   trajectories.append([agent.id, agent.id, agent.pos[0], agent.pos[1], agent.vel[0], agent.vel[1], birdRad, globalTime])

        drawWorld()
        if step == True:
            step = False
            paused = True    
        
        win.title('Hummingbirds at Feeder')
        win.after(framedelay, lambda: drawFrame(dt))
  

#=======================================================================================================================
# Main execution of the code
#=======================================================================================================================
#world_xmin, world_xmax, world_ymin, world_ymax = readScenario(scenarioFile)
world_xmin = 0
world_ymin = 0
world_xmax = 10
world_ymax = 10
world_width = world_xmax - world_xmin
world_height = world_ymax - world_ymin
world_scale = pixelsize/world_width

initialize()

# set the visualize
win = Tk()

# keyboard interaction
win.bind("<space>",on_key_press)
win.bind("s",on_key_press)
win.bind("<Escape>",on_key_press)
win.bind("v",on_key_press)

# the drawing canvas
canvas = Canvas(win, width=pixelsize, height=pixelsize*world_height/world_width, background="#666")
canvas.pack()
initWorld(canvas)
start_time = time.time()

# the main loop of the program
win.after(framedelay, lambda: drawFrame(dt))
mainloop()
if doExport:
    header = "id,gid,x,y,v_x,v_y,radius,time"
    exportFile = scenarioFile.split('.csv')[0] + "_sim.csv"
    np.savetxt(exportFile, trajectories, delimiter=",", fmt='%d,%d,%f,%f,%f,%f,%f,%f', header=header, comments='')


