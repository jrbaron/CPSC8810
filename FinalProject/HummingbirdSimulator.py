#Jessica Baron
#April 2018
#Maya script to manage simulating hummingbirds around a feeder.

import maya.cmds as maya
import numpy as np
from Hummingbird import Hummingbird

birds = []
numBirds = 3
birdRad = 0.2
feederPos = np.array([5, 5, 0])
feeder = Hummingbird(-1, feederPos, pb_rad=birdRad*2)

#---------------------------------------
def initialize():

    for i in range(numBirds):
        pos = np.array([3 + world_xmin + i,
                        3 + world_ymin + i,
                        0.0])
        vel = randomVector()
        #aggr = random.uniform(0.5, 4.0)   #Random float in given range
        aggr = random.random()
        pb_rad = birdRad
        ab_rad = pb_rad * ( 2*(1+aggr) )

        #Humm constructor sets additional properties like state.
        humm = Hummingbird(i, pos, vel, aggr, pb_rad, ab_rad, feederPos, colors[i%3])
        birds.append(humm)

#---------------------------------------
def updateSim():
    humm.computeForces(birds, ittr)

    humm.update(dt)

#---------------------------------------
def mainLoop():
    for timeSliderValue in range(1, 240, 5):
        maya.currentTime(timeSliderValue)

        updateSim()

        maya.select('camera1')
        maya.setKeyframe()

#---------------------------------------
initialize()
mainLoop()
