import random
import math
import numpy as np   #Vectors as np arrays
import maya.cmds as maya
#To import the Maya-commands library, the Maya Python executable must be set as the project's interpreter.
#For example, the file can be located at:  /Applications/Autodesk/maya2016/Maya.app/Contents/bin/mayapy

#----------------------------------
#Randomly generated, normalized vector
def randomVector():
    return np.random.uniform(-1, 1, size=3)

#Magnitude of a vector (which is a NumPy array).
def magnitude(vec):
    return np.sqrt(vec.dot(vec))

def normalize(vec, mag=None):
    if mag != None:
        m = mag
    else:
        m = magnitude(vec)

    if abs(m) > 0.00000001:
        return vec / m
    else:
        return vec

#----------------------------------
class Hummingbird():
    def __init__(self, i=-1, pos=np.zeros(3), vel=np.zeros(3), aggr=0.5,
                 pb_rad=1.0, ab_rad=1.5, goal=np.zeros(3), color="white"):

        self.id = i
        self.pos = pos
        self.prefSpeed = 1.0
        self.vel = vel * self.prefSpeed
        self.aggression = aggr
        self.PB_rad = pb_rad
        self.AB_rad = ab_rad
        #self.state = self.idleOrExplore()
        self.state = "seek"
        self.feedTime = 0
        self.color = color
        self.defaultColor = color

        self.timeHorizon = 10
        self.atGoal = False
        self.goalPos = goal
        self.feederPos = goal
        self.goalVel = self.goalPos - self.pos
        self.goalRadiusSq = 1.0
        self.maxSpeed = 5.0
        self.forces = np.zeros(3)
        self.goalForce = np.zeros(3)    #(self.goalVel - self.vel) / self.ksi
        self.maxForce = 10.0
        self.lastStateChangeFrame = 0

    #---------------------------------------
    def updateState(self, distToGoal, closestPos, frame, fps=24):
        num = random.random()
        timeSinceChange = frame-self.lastStateChangeFrame

        #Continue attacking?
        if self.state == "attack":
            if distToGoal > 2 or timeSinceChange > fps*5:
                self.state = "seek"
                self.color = self.defaultColor
                self.lastStateChangeFrame = frame
                self.goalPos = self.feederPos


        elif self.state == "seek":
            if timeSinceChange > fps*5:   #If at least 5 seconds passed since last state change

                if num*2 < self.aggression:   #Lower chance of attacking
                    self.state = "attack"
                    self.color = "red"
                    self.lastStateChangeFrame = frame
                    self.goalPos = closestPos

    #---------------------------------------
    def computeForces(self, neighbors, frame):
        dist = magnitude(self.goalPos-self.pos)
        avoid, closestPos = self.calcAvoidForce(neighbors)
        self.updateState(dist, closestPos, frame)   #Can change the goal

        if dist*dist > self.goalRadiusSq:
            self.goalForce = self.seek()
        else:
            self.goalForce = np.zeros(3)

        #Accumulate forces for each behavior.
        f = np.zeros(3)
        f += self.goalForce                          #First aim for the goal (whether feeder, random exploration destination, or another agent).

        if self.state != "attack":
            f += avoid

        #Restrict force by a maximum force.
        if magnitude(f) <= self.maxForce:
            self.forces = f
        else:
            self.forces = np.zeros(3)

    #-----------------------------
    def update(self, dt):

        #Update the velocity whether at goal or not but cap by the maximum speed.
        #newVel = self.vel + self.forces*dt
        #if magnitude(newVel) <= self.maxSpeed:
        #    self.vel = newVel
        self.vel = self.vel + (self.forces*dt)

        #Update position based on the new velocity.
        self.pos = self.pos + (self.vel*dt)

        #Compute the goal velocity for the next time step.
        #self.goalVel = self.goalPos - self.pos
        #distGoalSq = self.goalVel.dot(self.goalVel)
        #if distGoalSq < self.goalRadiusSq:
        #    self.atGoal = True
        #else:
        #    self.goalVel = (self.goalVel / math.sqrt(distGoalSq)) * self.prefSpeed


    #-----------------------------
    def calcAvoidForce(self, neighbors):
        f = np.zeros(3)
        dirToClosest = np.zeros(3)
        closestPos = np.zeros(3)
        lowestDist = float("inf")

        for n in neighbors:
            if n.id != self.id:
                ab_dir = self.pos-n.pos                         #Direction and magnitude from this Agent (a) to its neighbor (b).

                #Initial thought: Radius of the personal-bubble acts as radius used in collision detection.
                #Adjustment: Attack-bubble radius keeps the birds realistically further apart from each other.
                ab_dist = magnitude(ab_dir) - self.AB_rad - n.AB_rad
                ab_dir /= ab_dist                               #Normalize.

                if ab_dist < lowestDist:
                    dirToClosest = ab_dir
                    closestPos = n.pos

                #Radius of attack-bubble also acts as the sensing radius.
                if ab_dist < self.AB_rad:
                    t = self.timeToCollision(self.pos-n.pos,
                                             self.vel-n.vel,
                                             self.AB_rad+n.AB_rad)

                    if t is not None:
                        #Calculate new direction vector at time of predicted collision.
                        ab_dir = (self.pos + t*self.vel) - (n.pos + t*n.vel)
                        ab_dist = np.sqrt(ab_dir.dot(ab_dir))
                        ab_dir /= ab_dist

                        if t != 0:      #Don't divide by 0 (which is when there's already a collision).
                            avoidF = (max(self.timeHorizon-t, 0) / t) * ab_dir
                        else:
                            avoidF = max(self.timeHorizon, 0) * ab_dir

                        f += avoidF

        return f, closestPos

    #-----------------------------
    #Simple predictive time-to-collision of two agents using the quadratic formula to solve for a collision time.
    def timeToCollision(self, x, v, r):
        a = v.dot(v)
        b = x.dot(v)
        c = x.dot(x) - (r*r)
        d = (b*b) - (a*c)

        #If the magnitude of a is 0, then v is a zero vector, and both agents' velocities are the same (therefore colliding).
        if a == 0:
            return 0

        #If there is one solution (d<0) or one double-solution (d=0), no collision.
        if d <= 0:
            return None

        t_lower = (-b - np.sqrt(d)) / a
        t_upper = (-b + np.sqrt(d)) / a

        #If both solutions are negative, no collision.
        if t_lower < 0 and t_upper < 0:
            return None

        #If one is neg and the other non-neg, already colliding.
        if (t_lower < 0 and t_upper >= 0) or (t_upper < 0 and t_lower >= 0):
            return 0

        if (t_lower >= 0) and (t_upper >= 0):
            return min(t_lower, t_upper)        #Should be t_lower

    #-----------------------------
    #Returns a steering force that's used for aiming agent towards current goal.
    def seek(self):
        self.goalVel = self.goalPos - self.pos
        desiredVel = normalize(self.goalVel) * self.maxSpeed
        frc = desiredVel-self.vel
        return frc

    #-----------------------------
    def idleOrExplore(self):
        #num = random.random()  #Default in range 0.0 to 1.0.
        #if num < 0.5:
        #    self.state = "idle"
        #    self.vel = [0.0, 0.0, 0.0]
        #else:
        self.state = "explore"
        self.vel = randomVector()

    #-----------------------------
