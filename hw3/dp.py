#Jessica Baron
#CPSC 8810
#Homework 3: A*
#Spring 2018

# ----------
# Implement the function below.
#
# Compute the optimal path from start to goal.
# The car The car is moving on a 2D grid and
# its orientation can be chosen from four different directions:
forward = [[-1,  0], # 0: go up
           [ 0, -1], # 1: go left
           [ 1,  0], # 2: go down
           [ 0,  1]] # 3: go right

# The car can perform 3 actions: 0: right turn, 1: no turn, 2: left turn
action = [-1, 0, 1, 2]
action_name = ['R', 'F', 'L', 'D']
cost = [1, 1, 1, 1] # corresponding cost values

# name: (numerical representation, cost)
actionDict = {'R': (-1, 1), 'F': (0, 1), 'L': (1, 1), 'D': (2, 1)}

# GRID:
#     0 = navigable space
#     1 = unnavigable space 
grid = [[1, 1, 1, 0, 0, 0],
        [1, 1, 1, 0, 1, 0],
        [0, 0, 0, 0, 0, 0],
        [1, 1, 1, 0, 1, 1],
        [1, 1, 1, 0, 1, 1]]
maxRow = len(grid)
maxCol = len(grid[0])

start = [4, 3, 0] #[grid row, grid col, direction]
                
goal = [2, 0] #[grid row, grid col]



# ----------------------------------------
# modify code below
# ----------------------------------------

#-----------------------------------
def getNeighbors(x, y, orientation):
    neighbors = []

    fwd = forward[orientation]
    left = forward[(orientation+1)%4]
    right = forward[(orientation+3)%4]

    pos = (x+fwd[0], y+fwd[1])
    if pos[0] >= 0 and pos[1] >= 0:
        if pos[0] < maxRow and pos[1] < maxCol:
            neighbors.append((pos, 'F'))

    pos = (x+left[0], y+left[1])
    if pos[0] >= 0 and pos[1] >= 0:
        if pos[0] < maxRow and pos[1] < maxCol:
            neighbors.append((pos, 'L'))

    pos = (x+right[0], y+right[1])
    if pos[0] >= 0 and pos[1] >= 0:
        if pos[0] < maxRow and pos[1] < maxCol:
            neighbors.append((pos, 'R'))

    return neighbors

#-----------------------------------
def compute_policy(grid, start, goal, actionDict):
    value = [[[999 for row in range(len(grid[0]))] for col in range(len(grid))], 
             [[999 for row in range(len(grid[0]))] for col in range(len(grid))],
             [[999 for row in range(len(grid[0]))] for col in range(len(grid))],
             [[999 for row in range(len(grid[0]))] for col in range(len(grid))]]
    
    policy = [[[-1 for row in range(len(grid[0]))] for col in range(len(grid))], 
             [[-1  for row in range(len(grid[0]))] for col in range(len(grid))],
             [[-1 for row in range(len(grid[0]))] for col in range(len(grid))],
             [[-1 for row in range(len(grid[0]))] for col in range(len(grid))]]
    
    # your code: implement dynamic programming

    # Initially you may want to ignore orientation, that is, plan in 2D.
    # To do so, set actions=forward, cost = [1, 1, 1, 1], and action_name = ['U', 'L', 'R', 'D']
    # Similarly, set value=[[999 for row in range(len(grid[0]))] for col in range(len(grid))]
    # and policy = [[-1 for row in range(len(grid[0]))] for col in range(len(grid))]

    # return the optimal policy from the start.
    plan =[['-' for row in range(len(grid[0]))] for col in range(len(grid))]
    orientation = start[2]

    value = dynamicDijkstra(value, orientation)
    policy, plan = postProcessWithActions(value, policy, plan)

 #   print("")
 #   print("Value")
 #   show(value[0])
 #   print("")

 #   print("")
 #   print("Policy")
 #   show(policy[0])
 #   print("")

 #   print("")
 #   print("Plan")
 #   show(plan)
 #   print("")


    return plan

#-----------------------------------
def postProcessWithActions(values, policy, plan):
    for r in range(maxRow):
        for c in range(maxCol):

            if grid[r][c] != 1:

                for i in range(4):
                    name = action_name[(i+1)%4]
                    r_n = r + forward[i][0]
                    c_n = c + forward[i][1]

                    if r_n >= 0 and c_n >= 0:
                        if r_n < maxRow and c_n < maxCol:
                            if values[0][r_n][c_n] < values[0][r][c]:

                                policy[0][r][c] = actionDict[name][1]
                                plan[r][c] = name


    return policy, plan

#-----------------------------------
def dynamicDijkstra(value, startOrientation):

    orientation = startOrientation
    update = True
    while update:
        update = False

        for r in range(maxRow):
            for c in range(maxCol):

                #If at goal, set value to 0.
                if r == goal[0] and c == goal[1]:
                    if value[0][r][c] > 0:
                        value[0][r][c] = 0
                        update = True

                if grid[r][c] == 1:
                    value[0][r][c] = float("inf")

                else:
                    neighbors = getNeighbors(r, c, orientation)
                    for n in neighbors:
                        nPos = n[0]
                        action = n[1]
                        nOrientation = actionDict[action][0]
                        nCost = actionDict[action][1]

                        n_v = value[0][nPos[0]][nPos[1]] + nCost

                        #If potential, new cost is better than current, update.
                        if n_v < value[0][r][c]:
                            value[0][r][c] = n_v    #Update policy based on the action
                            update = True

                            orientation = nOrientation

    return value

#-----------------------------------
def show(p):
    for i in range(len(p)):
        print p[i]

#-----------------------------------
#Call here when testing via running dp.py.  Otherwise, this should be called from astar.py.
#plan = compute_policy(grid, start, goal, actionDict)
