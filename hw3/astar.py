#Jessica Baron
#CPSC 8810
#Homework 3: A*
#Spring 2018

import heapq   #Python priority queue
import dp

# ----------
# Implement the function below
#
# Compute the optimal path from start to goal.
# The car The car is moving on a 2D grid and
# its orientation can be chosen from four different directions:
#Indicates changing which row and column.
forward = [[-1,  0],    # 0: Go up (backwards one row).
           [ 0, -1],    # 1: Go left (backwards one column).
           [ 1,  0],    # 2: Go down (forward one row).
           [ 0,  1]]    # 3: Go right (forward one column).

# The car can perform 3 actions: 0: right turn, 1: no turn, 2: left turn
#action = [-1, 0, 1]         #Numerical representation of the actions that can be added to set new orientations.
#action_name = ['R', 'F', 'L']
#cost = [1, 1, 1] # corresponding cost values

# name: (numerical representation, cost, world-oriented index in forward[])
actionDict = {'R': (-1, 1, 3), 'F': (0, 1, 0), 'L': (1, 1, 1), 'D': (2, 1, 2)}

# GRID:
#     0 = navigable space
#     1 = unnavigable space 
grid = [[1, 1, 1, 0, 0, 0],
        [1, 1, 1, 0, 1, 0],
        [0, 0, 0, 0, 0, 0],
        [1, 1, 1, 0, 1, 1],
        [1, 1, 1, 0, 1, 1]]
maxRow = len(grid)
maxCol = len(grid[0])
print("Grid dimensions = ", maxRow, " rows, ", maxCol, "columns")

start = [4, 3, 0]   #[grid row, grid col, direction/orientation/theta]
goal = [2, 0]       #[grid row, grid col]

heuristic = [[2, 3, 4, 5, 6, 7],
        [1, 2, 3, 4, 5, 6],
        [0, 1, 2, 3, 4, 5],
        [1, 2, 3, 4, 5, 6],
        [2, 3, 4, 5, 6, 7]]

filler = '                    '
parent = [[[filler for row in range(len(grid[0]))] for col in range(len(grid))],
          [[filler for row in range(len(grid[0]))] for col in range(len(grid))],
          [[filler for row in range(len(grid[0]))] for col in range(len(grid))],
          [[filler for row in range(len(grid[0]))] for col in range(len(grid))]]


#--------------------------------
#Max of 3 neighbors in the cardinal directions (no diagonals) based on possible actions (up, left, right).
#Returns a list of tuples of (x,y) positions and action needed to get to that neighbor.
#Given the node's current orientation, how can it reach its neighbors? Which action is needed?
def getNeighbors(node):
    neighbors = []

    x = node[3][0]
    y = node[3][1]
    orientation = node[4]
    fwd = forward[orientation]
    left = forward[(orientation+1)%4]
    right = forward[(orientation+3)%4]

    pos = (x+fwd[0], y+fwd[1])
    if pos[0] >= 0 and pos[1] >= 0:
        if pos[0] < maxRow and pos[1] < maxCol:
            neighbors.append((pos, 'F'))

    pos = (x+left[0], y+left[1])
    if pos[0] >= 0 and pos[1] >= 0:
        if pos[0] < maxRow and pos[1] < maxCol:
            neighbors.append((pos, 'L'))

    pos = (x+right[0], y+right[1])
    if pos[0] >= 0 and pos[1] >= 0:
        if pos[0] < maxRow and pos[1] < maxCol:
            neighbors.append((pos, 'R'))

    return neighbors

#--------------------------------
#def compute_plan(grid, start, goal, cost, heuristic):
def compute_plan(grid, start, goal, actionDict, heuristic):

    # ----------------------------------------
    # modify the code below
    # ----------------------------------------
    closed = [[[0 for row in range(len(grid[0]))] for col in range(len(grid))], 
             [[0 for row in range(len(grid[0]))] for col in range(len(grid))],
             [[0 for row in range(len(grid[0]))] for col in range(len(grid))],
             [[0 for row in range(len(grid[0]))] for col in range(len(grid))]]

    plan =[['-' for row in range(len(grid[0]))] for col in range(len(grid))]

    #Indexing by dir/orientation, row, col
    closed[start[2]][start[0]][start[1]] = 1

    #2D versions without orientation.
    #closed_2D = [ [0 for row in range(len(grid[0]))] for col in range(len(grid)) ]
    #parent_2D = [ [('','') for row in range(len(grid[0]))] for col in range(len(grid)) ]

    x = start[0]
    y = start[1]
    theta = start[2]
    g = 0
    h = heuristic[x][y]
    f = g+h
    open = []

    #Each node item indexing:  (0, 1, 2, 3[0,1], 4)
    heapq.heappush(open, (f, g, h, (x, y), theta))

    # your code: implement A*
    # Initially you may want to ignore theta, that is, plan in 2D.
    # To do so, set actions=forward, cost = [1, 1, 1, 1], and action_name = ['U', 'L', 'R', 'D']
    # Similarly, set closed=[[0 for row in range(len(grid[0]))] for col in range(len(grid))]
    # and parent=[[' ' for row in range(len(grid[0]))] for col in range(len(grid))]


    node = heapq.heappop(open)
    pos = node[3]
    cnt = 0
    while (pos[0] != goal[0]) or (pos[1] != goal[1]):   #Comparing items of a tuple to a list (checking pos != goal is always false).

        #Pop off node with lowest with lowest f-cost (first value in the heap-item's tuple).
        print("Loop " + str(cnt))
        print("Current node = ", node)
        closed[theta][pos[0]][pos[1]] = 1

        neighbors = getNeighbors(node)
        nCnt = 0
        for neighbor in neighbors:
            #print("Neighbor "+str(nCnt)+" info = ", neighbor)
            nPos = neighbor[0]
            action = neighbor[1]
            nOrientation = actionDict[action][0]
            nCost = actionDict[action][1]
            new_theta = (theta+nOrientation) % 4

            if grid[nPos[0]][nPos[1]] == 1:    #Infinite cost for node on grid when there are obstacles.
                new_g = float("inf")
            else:
                new_g = node[1] + nCost

            #If neighbor not already visited.
            if closed[new_theta][nPos[0]][nPos[1]] == 0:

                h = heuristic[nPos[0]][nPos[1]]
                nNode = (new_g+h, new_g, h, nPos, new_theta)
                #print("\tnew_theta = " + str(new_theta))
                #print("\tnew_g + h = " + str(new_g + h))

                posInOpen = [n for n in open if nPos in n]
                if len(posInOpen) == 0:
                    print("\tNOT IN OPEN.")
                    heapq.heappush(open, nNode)
                    parent[new_theta][nPos[0]][nPos[1]] = node

                #If node already in queue and new g-cost is better than old, update that node's g- and f-cost.
                else:
                    print("\tIN OPEN.")
                    old_g = posInOpen[0][0]
                    i = open.index(posInOpen[0])
                    if new_g < old_g:
                        print("\tNew cost is better. Updating.")
                        open[i] = (new_g+h, new_g, h, nPos, new_theta)
                        parent[new_theta][nPos[0]][nPos[1]] = node

            nCnt += 1


        if (len(open) > 0):
            for itm in open:
                print(itm)
            node = heapq.heappop(open)
            pos = node[3]
            theta = node[4]
            cnt += 1
        else:
            print("FAILURE. Did not find goal node.")
            return plan

        print("")

    #If got this far, also add the last position (should be the goal) to the closed list.
    closed[theta][pos[0]][pos[1]] = 1


    print("")
    print("Closed")
    for t in range(len(closed)):
        print("theta = ", t)
        show(closed[t])
    print("")

    print("Parents")
    for t in range(len(parent)):
        print("theta = ", t)
        show(parent[t])
    print("")

    print("Number of loops = " + str(cnt))

    plan = walkPath(pos, theta, start, parent, plan)
    return plan

#--------------------------------
#Traverse the nodes starting from goal (the last node) through parents towards the start.
def walkPath(lastPos, lastTheta, start, parent, plan):
    path = []
    cnt = 0
    pos = lastPos
    theta = lastTheta
    while (pos[0] != start[0]) or (pos[1] != start[1]):
        path.insert(0, pos)
        #plan[pos[0]][pos[1]] = str(cnt)
        par = parent[theta][pos[0]][pos[1]]
        pos = par[3]
        theta = par[4]
        cnt += 1

    #Once more to set values at start node.
    path.insert(0, pos)

    #Now walk again and insert into final plan so numbering is ascending from start to end.
    cnt = 0
    for itm in path:
        x = itm[0]
        y = itm[1]
        plan[x][y] = str(cnt)
        cnt += 1


    return plan

#--------------------------------
def show(p):
    for i in range(len(p)):
        print p[i]

#--------------------------------
def computePlanWithPolicy(grid, start, goal, actionDict, heuristic, policy):
    plan =[['-' for row in range(len(grid[0]))] for col in range(len(grid))]

    pos = (start[0], start[1])
    theta = start[2]
    g = 0
    h = heuristic[pos[0]][pos[1]]
    f = g+h
    node = (f, g, h, pos, theta)


    while (pos[0] != goal[0]) or (pos[1] != goal[1]):   #Comparing items of a tuple to a list (checking pos != goal is always false).

        action = policy[pos[0]][pos[1]]
        if action in actionDict:
            i = actionDict[action][2]   #World orientation
            nCost = actionDict[action][1]
            nPos = (pos[0] + forward[i][0],  pos[1] + forward[i][1])
            h = heuristic[nPos[0]][nPos[1]]

            if grid[nPos[0]][nPos[1]] == 1:
                new_g = float("inf")
            else:
                new_g = node[1] + nCost

            nNode = (new_g+h, new_g, h, nPos, i)
            parent[i][nPos[0]][nPos[1]] = node
            pos = nNode[3]
            theta = nNode[4]
            node = nNode

    plan = walkPath(pos, theta, start, parent, plan)
    return plan

#--------------------------------

policy = dp.compute_policy(grid, start, goal, actionDict)

print("Start: " + str(start) + ", Goal: " + str(goal))

plan = compute_plan(grid, start, goal, actionDict, heuristic)
print("\nPlan (without DP Policy)")
show(plan)

print("\n~ ~ ~ ~ ~")

print("\nDynamic Programming Policy")
show(policy)
plan = computePlanWithPolicy(grid, start, goal, actionDict, heuristic, policy)
print("\nPlan (with DP Policy Look-ups)")
show(plan)


