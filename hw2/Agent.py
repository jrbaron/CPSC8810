import numpy as np

#Struct to keep track of data of an agent at one particular frame.

class Agent:
    def __init__(self, csvParameters):
        self.id = int(csvParameters[0])                 #The id of the agent
        self.gid = int(csvParameters[1])                #The group id of the agent
        self.pos = np.array([float(csvParameters[2]),
                             float(csvParameters[3])])  #The position of the agent
        self.vel = np.array([float(csvParameters[4]),
                             float(csvParameters[5])])  #The velocity of the agent
        self.radius = float(csvParameters[6])           #Disc radius (size estimate) of the agent
        self.frame = float(csvParameters[7])            #One particular frame of time

    #---------------------------------------
    #Simple predictive time-to-collision of two agents using the quadratic formula to solve for a collision time.
    def timeToCollision(self, other):
        x = self.pos - other.pos
        v = self.vel - other.vel
        r = self.radius + other.radius

        a = v.dot(v)
        b = x.dot(v)
        c = x.dot(x) - (r*r)
        d = (b*b) - (a*c)

        #If the magnitude of a is 0, then v is a zero vector, and both agents' velocities are the same (therefore colliding).
        if a == 0:
            return 0

        #If there is one solution (d<0) or one double-solution (d=0), no collision.
        if d <= 0:
            return None

        t_lower = (-b - np.sqrt(d)) / a
        t_upper = (-b + np.sqrt(d)) / a

        #If both solutions are negative, no collision.
        if t_lower < 0 and t_upper < 0:
            return None

        #If one is neg and the other non-neg, already colliding.
        if (t_lower < 0 and t_upper >= 0) or (t_upper < 0 and t_lower >= 0):
            return 0

        if (t_lower >= 0) and (t_upper >= 0):
            return min(t_lower, t_upper)        #Should be t_lower

    #---------------------------------------
