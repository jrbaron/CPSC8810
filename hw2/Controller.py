from Agent import Agent
from time import time
from random import randrange
import matplotlib.pyplot as plt
import numpy as np

frameDict = dict()      #frame-time : list of agents in frame
agentFramesDict = dict()    #agent.id : list of frames that agent is in
AgentsPerId = dict()        #agent.id : list of Agent objects corresponding to above dict
minFrame = 0.0
maxFrame = 0.0
numFrames = 0
maxTTC = 10.0
step = 0.04
invStep = int(100.0 / 4.0)
print("Frame step = ", step, ", 1/step = ", invStep)
pairwiseTtcList = []
scrambledTtcList = []

#-------------------------------------------
def readScenario(fileName):
    """
        Read a scenario from a file and update the dictionary of agents per frame.
    """
    global minFrame
    global maxFrame
    global numFrames

    fp = open(fileName, 'r')
    lines = fp.readlines()
    fp.close()
    for line in lines:
        params = line.split(',')
        if params[0] != "id":         #If first attrib is not in the first line as "id".
            agent = Agent(params)
            frame = agent.frame

            if frame not in frameDict:
                frameDict[frame] = [agent]
            else:
                frameDict[frame].append(agent)

            if agent.id not in agentFramesDict:
                agentFramesDict[agent.id] = [frame]
                AgentsPerId[agent.id] = [agent]
            else:
                agentFramesDict[agent.id].append(frame)
                AgentsPerId[agent.id].append(agent)

    minFrame = min(frameDict)
    maxFrame = max(frameDict)
    numFrames = len(frameDict)
    print("minFrame = ", minFrame)
    print("maxFrame = ", maxFrame)

#-------------------------------------------
def setPairwiseTtcList(outputFile, readFile=False):
    """
        Populate a list of all time-to-collisions per frame, per pair of agents within that frame.
        Also write these values to a file.
    """
    print("Start setPairwise()")
    global pairwiseTtcList

    if readFile:
        pairwiseTtcList = []
        fp = open(outputFile+".csv")
        for line in fp:
            pairwiseTtcList.append(float(line))

        fp.close()

    else:
        cnt = 0
        for frame in frameDict:
            agents = frameDict[frame]
            per = round(cnt/(float(numFrames)), 4)
            print("\t" + str(per) + "%")

            for agent in agents:
                for other in agents:
                    if agent.id != other.id:
                        t = agent.timeToCollision(other)
                        if t is not None and t < maxTTC:
                            pairwiseTtcList.append(t)
                            #print("\tttc = "+str(t))
            cnt += 1

        writeCsv(outputFile, pairwiseTtcList)
    print("End setPairwise()")

#-------------------------------------------
def setScrambledTtcList(outputFile, readFile=False):
    """
        Populate a list of all time-to-collisions per frame, per pair of agents within that frame.
    """
    print("Start setScrambled()")
    global scrambledTtcList

    if readFile:
        scrambledTtcList = []
        fp = open(outputFile+".csv")
        for line in fp:
            scrambledTtcList.append(float(line))

        fp.close()

    else:
        cnt = 0
        for frame in frameDict:
            per = round(cnt/(float(numFrames)), 4)
            print("\t" + str(per) + "%")

            agents = frameDict[frame]
            for agent in agents:
                scramAgent = timeScrambleAgent(frame, agent.id)
                for other in agents:
                    if other.id != agent.id:
                        scramOther = timeScrambleAgent(frame, other.id)
                        while scramAgent.id == scramOther.id:
                            scramOther = timeScrambleAgent(frame, other.id)

                        t = scramAgent.timeToCollision(scramOther)
                        if t is not None and t < maxTTC:
                            scrambledTtcList.append(t)
            cnt += 1

        writeCsv(outputFile, scrambledTtcList)
    print("End setScrambled()")

#-------------------------------------------
def timeScrambleAgent(frame, id):
    """

    :param frame: Time of the current frame.
    :param id: id of the agent to time-scramble.
    :return: An agent matching the given id but at a random time different from frame.
    """

    #print("Random frame = ", randomFrame)
    #return agent

    frames = agentFramesDict[id]
    agents = AgentsPerId[id]
    i = frames.index(frame)     #Position of given frame in the list of frames.
    while frames[i] == frame:   #Find a new, random position that's not that same frame.
        i = randrange(len(frames))
    randomAgent = agents[i]
    randomFrame = frames[i]

    #print("Agent "+str(id))
    #print("\tFrame = "+str(frame)+", Random frame = ", randomFrame)

    return randomAgent

#-------------------------------------------
def writeCsv(filename, timeList):
    fp = open(filename+".csv", 'w')
    for t in timeList:
        fp.write(str(t)+"\n")

    fp.close()

#-------------------------------------------
def displayHist(hist, name):
    x = hist[1]   #x and y axes of the histogram (TTCs and frequency, respectively)
    y = hist[0]
    fig, ax = plt.subplots()
    ax.plot(x[:-1], y)
    ax.set_title(name)
    plt.xlabel("Time-to-Collision (TTC) (Seconds)")
    plt.ylabel("Relative Frequency")
    fig.show()

#-------------------------------------------
def pairDistribFunct(pairHist, scramHist):

    binEdges = pairHist[1]
    ratioFreqs = np.zeros(len(binEdges)-1)
    pairFreqs = pairHist[0]
    scramFreqs = scramHist[0]

    scale = float(len(pairwiseTtcList)) / len(scrambledTtcList)

    for i in range(len(binEdges)-1):
        denom = scramFreqs[i]
        if denom != 0:
            freq = pairFreqs[i] / scramFreqs[i]
            ratioFreqs[i] = scale*freq

    plt.plot(binEdges[:-1], ratioFreqs)
    plt.title("Pair-Distribution Function")
    plt.ylabel("Ratio (pairwise/time-scrambled)")
    plt.xlabel("Time-to-Collision (TTC) (Seconds)")
    plt.show()

    return ratioFreqs

#-------------------------------------------
def interactionEnergy(bins, pairDistrib):
    energies = np.log(np.divide(1.0, pairDistrib))

    plt.plot(bins[:-1], energies)

    plt.title("Interaction Energy")
    plt.ylabel("Power (Arb units)")
    plt.xlabel("Time-to-Collision (TTC) (Seconds)")
    plt.show()

#-------------------------------------------
def makeHistograms():
    test = False
    if test:
        arr = [0,1,2,3,4,5,5,6]
        y, x = np.histogram(arr)
        fig, ax = plt.subplots()
        ax.plot(x[:-1], y)
        fig.show()

    bins = np.arange(start=0, stop=maxTTC, step=0.1)

    if len(pairwiseTtcList) > 0:
        pairwiseHist = np.histogram(pairwiseTtcList, density=True, bins=bins)  #True density normalizes.
        displayHist(pairwiseHist, "Pairwise TTCs")

    if len(scrambledTtcList) > 0:
        scrambledHist = np.histogram(scrambledTtcList, density=True, bins=bins)
        displayHist(scrambledHist, "Time-Scrambled TTCs")

    pairDistrib = pairDistribFunct(pairwiseHist, scrambledHist)

    interactionEnergy(bins, pairDistrib)

#-----------------------------------------------------
if __name__ == "__main__":
    runtime = time()
    readScenario("campus.csv")
    runtime = time() - runtime
    print("readScenario() runtime = "+ str(runtime) +" seconds")

    runtime = time()
    setPairwiseTtcList("PairwiseTTCs", True)
    #setPairwiseTtcList("PairwiseTTCs")
    runtime = time() - runtime
    print("setPairwiseTtcList() runtime = "+ str(runtime) +" seconds")

    runtime = time()
    setScrambledTtcList("TimeScrambledTTCs", True)
    #setScrambledTtcList("TimeScrambledTTCs")
    runtime = time() - runtime
    print("setScrambledTtcList() runtime = "+ str(runtime) +" seconds")

    runtime = time()
    pairDistribHist = makeHistograms()
    runtime = time() - runtime
    print("makeHistograms() runtime = "+ str(runtime) +" seconds")

